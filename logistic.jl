using PyPlot
using Distributions
using Colors

r=3
f(r,x) = r*x*(1-x)
f(x) = f(r,x)

g(x)=2/(x+2)
function logistics(f,x0,N)
    xs=[x0]
    for n = 1:N
        println(f(xs[n]))
        push!(xs,f(xs[n]))
    end
    #pyplot()
    c0 = rand(Uniform(0.0,1.0),3,1)
    scatter(collect(1:N+1),xs,color=rand(Uniform(0.0,1.0),1,3))
end
rs = range(2.9,length=80,stop=3.99)
k=1
ioff()
for r in rs
    fig = figure(k,figsize=(15,10));
    logistics(g,-0.3,500);
    legend(["r=$r"]);
    savefig("ric$k.png");
    global k+=1
    println(k)
    close(fig)
end