using PyPlot
#using Distributions
#using Colors
ra=3
f(x) = x^3-2*x^2-11*x+12
fd(x) = 3*x^2-4*x-11

g(x)=2/(x+2)
function newton(f,x0,N)
    xs=[x0]
    for n = 1:N
        x = xs[n]
        y = x-f(x)/fd(x)
        println("xn= ",y," f(xn)= ",f(y))
        push!(xs,y)
    end
    pygui(true)
    zeros = [0 for _ in 1:N+1]
    scatter(0:N,xs, marker=".")
end
